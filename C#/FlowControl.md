### Flow Control
 -  Avoid invoking methods within a conditional expression.
 -  Avoid creating recursive methods. Use loops or nested loops instead.
 -  Avoid using foreach to iterate over immutable value-type collections. E.g. String arrays.
 -  Do not modify enumerated items within a foreach statement.
 -  Use the ternary conditional operator only for trivial conditions. Avoid complex or compound ternary operations.
Example: int result = isValid ? 9 : 4;
 -  Avoid evaluating Boolean conditions against true or false.
Example:
```
// Bad!
if (isValid == true)
{…}
// Good!
if (isValid)
{…}
```
 -  Avoid assignment within conditional statements.
Example: if((i=2)==2) {…}
 -  Avoid compound conditional expressions – use Boolean variables to split parts into multiple manageable
expressions.
Example:
```
// Bad!
if (((value > _highScore) && (value != _highScore)) && (value < _maxScore))
{…}
// Good!
isHighScore = (value >= _highScore);
isTiedHigh = (value == _highScore);
isValid = (value < _maxValue);
if ((isHighScore && ! isTiedHigh) && isValid)
{…}
```
 -  Avoid explicit Boolean tests in conditionals.
```
Example:
// Bad!
if(IsValid == true)
{…};
// Good!
if(IsValid)
{…}
```
 -  Only use switch/case statements for simple operations with parallel conditional logic.
 -  Prefer nested if/else over switch/case for short conditional sequences and complex conditions.
 -  Prefer polymorphism over switch/case to encapsulate and delegate complex operations.

### Exceptions
 -  Do not use try/catch blocks for flow-control.
 -  Only catch exceptions that you can handle.
 -  Never declare an empty catch block.
 -  Avoid nesting a try/catch within a catch block.
 -  Always catch the most derived exception via exception filters.
 -  Order exception filters from most to least derived exception type.
 -  Avoid re-throwing an exception. Allow it to bubble-up instead.
 -  If re-throwing an exception, preserve the original call stack by omitting the exception argument from the throw
statement.
Example:
```
// Bad!
catch(Exception ex)
{
 Log(ex);
throw ex;
}
// Good!
catch(Exception)
{
 Log(ex);
throw;
}
```
 -  Only use the finally block to release resources from a try statement. 
 -  Always use validation to avoid exceptions.
Example:
```
// Bad!
try
{
 conn.Close();
}
Catch(Exception ex)
{
 // handle exception if already closed!
}
// Good!
if(conn.State != ConnectionState.Closed)
{
 conn.Close();
}
```
 -  Always set the innerException property on thrown exceptions so the exception chain & call stack are
maintained.
 -  Avoid defining custom exception classes. Use existing exception classes instead.
 -  When a custom exception is required;
a. Always derive from Exception not ApplicationException.
b. Always suffix exception class names with the word “Exception”.
c. Always add the SerializableAttribute to exception classes.
d. Always implement the standard “Exception Constructor Pattern”:
public MyCustomException ();
public MyCustomException (string message);
public MyCustomException (string message, Exception innerException);
e. Always implement the deserialization constructor:
protected MyCustomException(SerializationInfo info, StreamingContext contxt);
 -  Always set the appropriate HResult value on custom exception classes.
(Note: the ApplicationException HResult = -2146232832)
 -  When defining custom exception classes that contain additional properties:
a. Always override the Message property, ToString() method and the implicit operator string
to include custom property values.
b. Always modify the deserialization constructor to retrieve custom property values.
c. Always override the GetObjectData(…) method to add custom properties to the serialization collection.
Example:
```
public override void GetObjectData(SerializationInfo info,
 StreamingContext context)
{
 base.GetObjectData (info, context);
info.AddValue("MyValue", _myValue);
} 
```