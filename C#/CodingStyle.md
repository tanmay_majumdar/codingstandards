## Coding Style
Coding style causes the most inconsistency and controversy between developers. Each developer has a preference, and
rarely are two the same. However, consistent layout, format, and organization are key to creating maintainable code.
The following sections describe the preferred way to implement C# source code in order to create readable, clear, and
consistent code that is easy to understand and maintain.

### General Language Guidelines
1. Do not omit access modifiers. Explicitly declare all identifiers with the appropriate access modifier instead of
allowing the default.
Example:
```
// Bad!
Void WriteEvent(string message)
{…}
// Good!
private Void WriteEvent(string message)
{…}
```
2. Do not use the default (“1.0.*”) versioning scheme. Increment the AssemblyVersionAttribute value
manually.
3. Set the ComVisibleAttribute to false for all assemblies.
4. Only selectively enable the ComVisibleAttribute for individual classes when needed.
Example:
```
[assembly: ComVisible(false)]
[ComVisible(true)]
public MyClass
{…}
```
5. Consider factoring classes containing unsafe code blocks into a separate assembly.
6. Avoid mutual references between assemblies.
4.2 Variables & Types
7. Try to initialize variables where you declare them.
8. Always choose the simplest data type, list, or object required.
9. Always use the built-in C# data type aliases, not the .NET common type system (CTS).
Example:
```
short NOT System.Int16
int NOT System.Int32
long NOT System.Int64
string NOT System.String
```
10. Only declare member variables as private. Use properties to provide access to them with public,
protected, or internal access modifiers.
11. Try to use int for any non-fractional numeric values that will fit the int datatype - even variables for nonnegative
numbers.
12. Only use long for variables potentially containing values too large for an int.
13. Try to use double for fractional numbers to ensure decimal precision in calculations.
14. Only use float for fractional numbers that will not fit double or decimal.
15. Avoid using float unless you fully understand the implications upon any calculations.
16. Try to use decimal when fractional numbers must be rounded to a fixed precision for calculations. Typically
this will involve money.
17. Avoid using sbyte, short, uint, and ulong unless it is for interop (P/Invoke) with native libraries. 
 Avoid specifying the type for an enum - use the default of int unless you have an explicit need for long (very
uncommon).
19. Avoid using inline numeric literals (magic numbers). Instead, use a Constant or Enum.
20. Avoid declaring string literals inline. Instead use Resources, Constants, Configuration Files, Registry or other
data sources.
21. Declare readonly or static readonly variables instead of constants for complex types.
22. Only declare constants for simple types.
23. Avoid direct casts. Instead, use the “as” operator and check for null.
Example:
```
object dataObject = LoadData();
DataSet ds = dataObject as DataSet;
if(ds != null)
{…}
```
24. Always prefer C# Generic collection types over standard or strong-typed collections.
25. Always explicitly initialize arrays of reference types using a for loop.
26. Avoid boxing and unboxing value types.
Example:
```
int count = 1;
object refCount = count; // Implicitly boxed.
int newCount = (int)refCount; // Explicitly unboxed.
```
27. Floating point values should include at least one digit before the decimal place and one after.
Example: totalPercent = 0.05;
28. Try to use the “@” prefix for string literals instead of escaped strings.
29. Prefer String.Format() or StringBuilder over string concatenation.
30. Never concatenate strings inside a loop.
31. Do not compare strings to String.Empty or “” to check for empty strings. Instead, compare by using
String.Length == 0.
32. Avoid hidden string allocations within a loop. Use String.Compare() for case-sensitive 
Example (ToLower() create a temp string)

```
// Bad!
int id = -1;
string name = “john doe”;
for(int i=0; i < customerList.Count; i++)
{
if(customerList[i].Name.ToLower() ToLower() ToLower() == name)
 {
 id = customerList[i].ID;
 }
}
// Good!
int id = -1;
string name = “john doe”;
for(int i=0; i < customerList.Count; i++)
{
// The “ignoreCase = true” argument performs a
 // case-insensitive compare without new allocation.
if(String.Compare String.Compare String.Compare(customerList[i].Name, name, true)== 0)
 {
 id = customerList[i].ID;
 }
}
```

### Formatting
1. Never declare more than 1 namespace per file.
2. Avoid putting multiple classes in a single file.
3. Always place curly braces ({ and }) on a new line.
4. Always use curly braces ({ and }) in conditional statements.
5. Always use a Tab & Indention size of 4.
6. Declare each variable independently – not in the same statement.
7. Place namespace “using” statements together at the top of file. Group .NET namespaces above custom
namespaces.
8. Group internal class implementation by type in the following order:
a. Member variables.
b. Constructors & Finalizers.
c. Nested Enums, Structs, and Classes.
d. Properties
e. Methods
9. Sequence declarations within type groups based upon access modifier and visibility:
a. Public
b. Protected
c. Internal
d. Private
10. Segregate interface Implementation by using #region statements.
11. Append folder-name to namespace for source files within sub-folders.
12. Recursively indent all code blocks contained within braces.
13. Use white space (CR/LF, Tabs, etc) liberally to separate and organize code.
14. Only declare related attribute declarations on a single line, otherwise stack each attribute as a separate
declaration.
Example:
```
// Bad!
[Attrbute1, Attrbute2, Attrbute3]
public class MyClass
{…}
// Good!
[Attrbute1, RelatedAttribute2]
[Attrbute3]
[Attrbute4]
public class MyClass
{…}
```
15. Place Assembly scope attribute declarations on a separate line.
16. Place Type scope attribute declarations on a separate line.
17. Place Method scope attribute declarations on a separate line.
18. Place Member scope attribute declarations on a separate line.
19. Place Parameter attribute declarations inline with the parameter.
20. If in doubt, always err on the side of clarity and consistency. 

### Code Commenting
21. All comments should be written in the same language, be grammatically correct, and contain appropriate
punctuation.
22. Use // or /// but never /* … */
23. Do not “flowerbox” comment blocks.
Example:
```
 // ***************************************
 // Comment block
 // ***************************************
 ```
24. Use inline-comments to explain assumptions, known issues, and algorithm insights.
25. Do not use inline-comments to explain obvious code. Well written code is self documenting.
26. Only use comments for bad code to say “fix this code” – otherwise remove, or rewrite the code!
27. Include comments using Task-List keyword flags to allow comment-filtering.
Example:
```
// TODO: Place Database Code Here
// UNDONE: Removed P\Invoke Call due to errors
// HACK: Temporary fix until able to refactor
```
28. Always apply C# comment-blocks (///) to public, protected, and internal declarations.
29. Only use C# comment-blocks for documenting the API.
30. Always include <summary> comments. Include `<param>`, `<return>`, and `<exception>` comment
sections where applicable.
31. Include `<see cref=""/>` and `<seeAlso cref=""/>` where possible.
32. Always add CDATA tags to comments containing code and other embedded markup in order to avoid
encoding issues.
Example:
```
/// <example>
/// Add the following key to the “appSettings” section of your config:
/// <code><![CDATA[
/// <configuration>
/// <appSettings>
/// <add key="mySetting" value="myValue"/>
/// </appSettings>
/// </configuration>
/// ]]></code> >
/// </example> 
```