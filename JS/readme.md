# JavaScript Style Guide

## Table of Contents

  1. [Types](#types)
  1. [References](#references)
  1. [Objects](#objects)
  1. [Arrays](#arrays)
  1. [Destructuring](/JS/destructuring_and_strings.md)
  1. [Strings](/JS/destructuring_and_strings.md)
  1. [Functions](/JS/functions.md)
  1. [Arrow Functions](/JS/functions.md)
  1. [Classes & Constructors](/JS/classes_and_modules.md)
  1. [Modules](/JS/classes_and_modules.md)
  1. [Iterators and Generators](/JS/iterations_generator_and_properties.md)
  1. [Properties](/JS/iterations_generator_and_properties.md)
  1. [Variables](/JS/vairbales_and_hoisting.md)
  1. [Hoisting](/JS/vairbales_and_hoisting.md)
  1. [Comparison Operators & Equality](/JS/operators_and_blocks.md)
  1. [Blocks](/JS/operators_and_blocks.md)
  1. [Control Statements](/JS/controlStatements_and_comments.md)
  1. [Comments](/JS/controlStatements_and_comments.md)
  1. [Whitespace](/JS/whitespace_till_namingConventions.md)
  1. [Commas](/JS/whitespace_till_namingConventions.md)
  1. [Semicolons](/JS/whitespace_till_namingConventions.md)
  1. [Type Casting & Coercion](/JS/whitespace_till_namingConventions.md)
  1. [Naming Conventions](/JS/whitespace_till_namingConventions.md)
  1. [Accessors](/JS/accessors_and_events.md)
  1. [Events](/JS/accessors_and_events.md)
  1. [Testing](/JS/testing_and_performance.md)
  1. [Performance](/JS/testing_and_performance.md)

## Types

  - [1.1](#types) **Primitives**: When you access a primitive type you work directly on its value.

    - string
    - number
    - boolean
    - null
    - undefined
    - symbol


```
const foo = 1;
let bar = foo;

bar = 9;

console.log(foo, bar); // => 1, 9
```

    - Symbols cannot be faithfully polyfilled, so they should not be used when targeting browsers/environments that don’t support them natively.

  
  - [1.2] **Complex**: When you access a complex type you work on a reference to its value.

    - `object`
    - `array`
    - `function`

```
    const foo = [1, 2];
    const bar = foo;

    bar[0] = 9;

    console.log(foo[0], bar[0]); // => 9, 9
```

## References

  - [2.1](#references) Use `const` for all of your references; avoid using `var`. eslint: [`prefer-const`](https://eslint.org/docs/rules/prefer-const.html), [`no-const-assign`](https://eslint.org/docs/rules/no-const-assign.html)

    > Why? This ensures that you can’t reassign your references, which can lead to bugs and difficult to comprehend code.

```javascript
    // bad
    var a = 1;
    var b = 2;

    // good
    const a = 1;
    const b = 2;
```

  - [2.2](#references--variable) If you must reassign references, use `let` instead of `var`. eslint: [`no-var`](https://eslint.org/docs/rules/no-var.html)

    > Why? `let` is block-scoped rather than function-scoped like `var`.

```javascript
    // bad
    var count = 1;
    if (true) {
      count += 1;
    }

    // good, use the let.
    let count = 1;
    if (true) {
      count += 1;
    }
```

  - [2.3](#references--let) Note that both `let` and `const` are block-scoped.

```javascript
    // const and let only exist in the blocks they are defined in.
    {
      let a = 1;
      const b = 1;
    }
    console.log(a); // ReferenceError
    console.log(b); // ReferenceError
```

**[⬆ back to top](#table-of-contents)**

## Objects

  - [3.1](#objects) Use the literal syntax for object creation. eslint: [`no-new-object`](https://eslint.org/docs/rules/no-new-object.html)

```javascript
    // bad
    const item = new Object();

    // good
    const item = {};
```

  - [3.2](#objects--propertyname) Use computed property names when creating objects with dynamic property names.

    > Why? They allow you to define all the properties of an object in one place.

```javascript

    function getKey(k) {
      return `a key named ${k}`;
    }

    // bad
    const obj = {
      id: 5,
      name: 'San Francisco',
    };
    obj[getKey('enabled')] = true;

    // good
    const obj = {
      id: 5,
      name: 'San Francisco',
      [getKey('enabled')]: true,
    };
```

  - [3.3](#objects--shorthand) Use object method shorthand. eslint: [`object-shorthand`](https://eslint.org/docs/rules/object-shorthand.html)

```javascript
    // bad
    const atom = {
      value: 1,

      addValue: function (value) {
        return atom.value + value;
      },
    };

    // good
    const atom = {
      value: 1,

      addValue(value) {
        return atom.value + value;
      },
    };
```

  - [3.4](#objects-property-shorthand) Use property value shorthand. eslint: [`object-shorthand`](https://eslint.org/docs/rules/object-shorthand.html)

    > Why? It is shorter and descriptive.

```javascript
    const lukeSkywalker = 'Luke Skywalker';

    // bad
    const obj = {
      lukeSkywalker: lukeSkywalker,
    };

    // good
    const obj = {
      lukeSkywalker,
    };
```

  - [3.5](#objects-group-shorthand) Group your shorthand properties at the beginning of your object declaration.

    > Why? It’s easier to tell which properties are using the shorthand.

```javascript
    const anakinSkywalker = 'Anakin Skywalker';
    const lukeSkywalker = 'Luke Skywalker';

    // bad
    const obj = {
      episodeOne: 1,
      twoJediWalkIntoACantina: 2,
      lukeSkywalker,
      episodeThree: 3,
      mayTheFourth: 4,
      anakinSkywalker,
    };

    // good
    const obj = {
      lukeSkywalker,
      anakinSkywalker,
      episodeOne: 1,
      twoJediWalkIntoACantina: 2,
      episodeThree: 3,
      mayTheFourth: 4,
    };
```

  - [3.6](#objects-quoteprops) Only quote properties that are invalid identifiers. eslint: [`quote-props`](https://eslint.org/docs/rules/quote-props.html)

    > Why? In general we consider it subjectively easier to read. It improves syntax highlighting, and is also more easily optimized by many JS engines.

```javascript
    // bad
    const bad = {
      'foo': 3,
      'bar': 4,
      'data-blah': 5,
    };

    // good
    const good = {
      foo: 3,
      bar: 4,
      'data-blah': 5,
    };
```

  - [3.7](#objects-objectprototype) Do not call `Object.prototype` methods directly, such as `hasOwnProperty`, `propertyIsEnumerable`, and `isPrototypeOf`. eslint: [`no-prototype-builtins`](https://eslint.org/docs/rules/no-prototype-builtins)

    > Why? These methods may be shadowed by properties on the object in question - consider `{ hasOwnProperty: false }` - or, the object may be a null object (`Object.create(null)`).

```javascript
    // bad
    console.log(object.hasOwnProperty(key));

    // good
    console.log(Object.prototype.hasOwnProperty.call(object, key));

    // best
    const has = Object.prototype.hasOwnProperty; // cache the lookup once, in module scope.
    console.log(has.call(object, key));
    /* or */
    import has from 'has'; // https://www.npmjs.com/package/has
    console.log(has(object, key));
```

  - [3.8](#objects-spreadop) Prefer the object spread operator over [`Object.assign`](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/assign) to shallow-copy objects. Use the object rest operator to get a new object with certain properties omitted.

```javascript
    // very bad
    const original = { a: 1, b: 2 };
    const copy = Object.assign(original, { c: 3 }); // this mutates `original` ಠ_ಠ
    delete copy.a; // so does this

    // bad
    const original = { a: 1, b: 2 };
    const copy = Object.assign({}, original, { c: 3 }); // copy => { a: 1, b: 2, c: 3 }

    // good
    const original = { a: 1, b: 2 };
    const copy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }

    const { a, ...noA } = copy; // noA => { b: 2, c: 3 }
```

**[⬆ back to top](#table-of-contents)**
